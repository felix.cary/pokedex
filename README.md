A Pokedex app

**This application has been tested on:**
- Android Redmi 10 (Android 12, MIUI 13)
- Android Studio Emulator (SDK 30)

**Instruction to build and run app:**
- Download the project and open using visual studio code
- Go to pubspec.yaml and run "flutter pub get" to download required library
- Run it on simulator or real device

**Feature and Tech Stack:**
- Pages:
    - Pokemon List
    - Pokemon Detail Page
- BLoC State Management (Cubit)
- Dio for HTTP Request
- Fluro for route
- Dependency Injection using GetIt
- Image loading and caching

**Version Tools**
Flutter 3.10.5
Dart 3.0.5