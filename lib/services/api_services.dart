import 'package:dio/dio.dart';
import 'package:pokedex/model/pokemon_detail_model.dart';
import 'package:pokedex/model/pokemon_list_model.dart';

class PokemonApiService {
  final Dio dioHttp;

  PokemonApiService({required this.dioHttp});

  Future<List<Pokemon>> getPokemonList({
    required int pageSize,
    required int currentPage,
  }) async {
    final response = await dioHttp.get(
        'https://pokeapi.co/api/v2/pokemon?limit=$pageSize&offset=${(currentPage - 1) * pageSize}');

    final List<Pokemon> pokemons = (response.data['results'] as List)
        .map((data) => Pokemon.fromJson(data))
        .toList();

    return pokemons;
  }

  Future<PokemonDetail> getPokemonDetail({
    required String name,
  }) async {
    final response =
        await dioHttp.get('https://pokeapi.co/api/v2/pokemon/$name');

    final PokemonDetail pokemon = PokemonDetail.fromMap(response.data);

    return pokemon;
  }
}
