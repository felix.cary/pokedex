part of '../pokemon_list_screen.dart';

class PokemonListItem extends StatelessWidget {
  final Pokemon pokemon;

  const PokemonListItem({
    super.key,
    required this.pokemon,
  });

  @override
  Widget build(BuildContext context) {
    final parsedUrl = StringUtil.getParsedUrl(pokemon.url);

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black54),
        borderRadius: BorderRadius.circular(16),
      ),
      child: InkWell(
        onTap: () {
          App().router.navigateTo(
                context,
                pokemonDetailRoute.name,
                routeSettings: RouteSettings(
                  arguments: {
                    'pokemonName': pokemon.name,
                  },
                ),
              );
        },
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: Text(
                  StringUtil().getPokemonNumber(int.parse(parsedUrl[1])),
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black87,
                  ),
                ),
              ),
              Text(
                pokemon.name.capitalize(),
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Colors.black87,
                ),
              ),
              Expanded(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: CachedImage(
                      imageUrl: StringUtil().getImageUrl(
                        int.parse(parsedUrl[1]),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
