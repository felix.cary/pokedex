part of '../pokemon_detail_screen.dart';

class PokemonDetailTab extends StatelessWidget {
  final PokemonDetail pokemon;

  const PokemonDetailTab({super.key, required this.pokemon});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: DefaultTabController(
        length: 4,
        child: Column(
          children: [
            TabBar(
              labelPadding: EdgeInsets.zero,
              labelColor: ColorUtil.getTypeColor(
                pokemon.types!.first.type.name,
              ),
              indicatorColor: ColorUtil.getTypeColor(
                pokemon.types!.first.type.name,
              ),
              labelStyle: const TextStyle(
                fontWeight: FontWeight.w600,
              ),
              unselectedLabelColor: Colors.black,
              tabs: const [
                Tab(text: 'About'),
                Tab(text: 'Base Stats'),
                Tab(text: 'Evolution'),
                Tab(text: 'Moves'),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  PokemonDetailTabAbout(pokemon: pokemon),
                  PokemonDetailTabStats(pokemon: pokemon),
                  const Center(child: Text('Evolution')),
                  const Center(child: Text('Moves')),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
