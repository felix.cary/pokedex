part of '../pokemon_detail_screen.dart';

class PokemonDetailTabAbout extends StatelessWidget {
  final PokemonDetail pokemon;

  const PokemonDetailTabAbout({super.key, required this.pokemon});

  @override
  Widget build(BuildContext context) {
    List<String> abilityNames = pokemon.abilities!
        .map((ability) => StringUtil().capitalizeWords(ability.ability.name))
        .toList();
    String joinedAbilities = abilityNames.join(', ');

    return Column(
      children: [
        const SizedBox(height: 16),
        _aboutPokemonWidget(
          title: "Spesies",
          info:
              '${StringUtil().capitalizeWords(pokemon.types?.first.type.name ?? '-')} Pokemon',
        ),
        _aboutPokemonWidget(
          title: "Height",
          info: "${pokemon.height! / 10} m",
        ),
        _aboutPokemonWidget(
          title: "Weight",
          info: "${pokemon.weight! / 10} kg",
        ),
        _aboutPokemonWidget(
          title: "Abilities",
          info: joinedAbilities,
        ),
      ],
    );
  }

  Widget _aboutPokemonWidget({
    required String title,
    required String info,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(
              title,
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.black87,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              info,
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.black87,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
