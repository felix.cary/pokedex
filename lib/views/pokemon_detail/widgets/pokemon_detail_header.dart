part of '../pokemon_detail_screen.dart';

class PokemonDetailHeader extends StatelessWidget {
  final PokemonDetail pokemon;

  const PokemonDetailHeader({super.key, required this.pokemon});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              pokemon.name!.capitalize(),
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 28,
                color: Colors.white,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: List.generate(
                pokemon.types!.length,
                (index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: Chip(
                      label: Text(
                        pokemon.types![index].type.name.capitalize(),
                        style: const TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                      backgroundColor: ColorUtil.getTypeColor(
                        pokemon.types!.first.type.name,
                      ).withOpacity(0.6),
                    ),
                  );
                },
              ),
            )
          ],
        ),
        Text(
          StringUtil().getPokemonNumber(pokemon.id!),
          style: const TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
