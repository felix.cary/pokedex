part of '../pokemon_detail_screen.dart';

class PokemonDetailBackground extends StatelessWidget {
  final PokemonDetail pokemon;

  const PokemonDetailBackground({super.key, required this.pokemon});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorUtil.getTypeColor(
        pokemon.types!.first.type.name,
      ),
      width: double.infinity,
      height: double.infinity,
    );
  }
}
