part of '../pokemon_detail_screen.dart';

class PokemonDetailTabStats extends StatelessWidget {
  final PokemonDetail pokemon;

  const PokemonDetailTabStats({super.key, required this.pokemon});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
        pokemon.stats!.length,
        (index) {
          return _baseStatPokemonWidget(
            title:
                StringUtil().capitalizeWords(pokemon.stats![index].stat.name),
            stat: pokemon.stats![index].baseStat,
          );
        },
      ),
    );
  }

  Widget _baseStatPokemonWidget({
    required String title,
    required int stat,
  }) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 4,
            child: Text(
              title,
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.black87,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              stat.toString(),
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.black87,
              ),
            ),
          ),
          Expanded(
            flex: 6,
            child: _statusBar(fillNumber: stat),
          ),
        ],
      ),
    );
  }

  Widget _statusBar({
    required fillNumber,
    int maxNumber = 255,
  }) {
    double fillRatio = fillNumber / maxNumber;
    Color color = AppColors.statusBarTosca;
    if (fillRatio < 0.25) {
      color = AppColors.statusBarPink;
    } else if (fillRatio < 0.5) {
      color = AppColors.statusBarYellow;
    }

    return LinearProgressIndicator(
      value: fillRatio,
      backgroundColor: AppColors.statusBarGrey,
      valueColor: AlwaysStoppedAnimation<Color>(color),
    );
  }
}
