import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokedex/bloc/pokemon_detail/pokemon_detail_cubit.dart';
import 'package:pokedex/model/pokemon_detail_model.dart';
import 'package:pokedex/styles/colors.dart';
import 'package:pokedex/utils/color_utils.dart';
import 'package:pokedex/utils/string_util.dart';
import 'package:pokedex/widgets/cached_image.dart';

part './widgets/pokemon_detail_background.dart';
part './widgets/pokemon_detail_header.dart';
part './widgets/pokemon_detail_tab_background.dart';
part './widgets/pokemon_detail_tab.dart';
part './widgets/pokemon_detail_tab_about.dart';
part './widgets/pokemon_detail_tab_stats.dart';

class PokemonDetailScreen extends StatelessWidget {
  const PokemonDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocBuilder<PokemonDetailCubit, PokemonDetailState>(
          builder: (context, state) {
            if (state.isLoading) {
              return Column(
                children: [
                  AppBar(
                    iconTheme: const IconThemeData(color: Colors.black),
                    backgroundColor: Colors.white,
                    elevation: 0,
                  ),
                  const Expanded(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ],
              );
            } else if (state.pokemon == null) {
              return Column(
                children: [
                  AppBar(
                    iconTheme: const IconThemeData(color: Colors.black),
                    backgroundColor: Colors.white,
                    elevation: 0,
                  ),
                  const Expanded(
                      child: Center(child: Text('Pokemon not found.'))),
                ],
              );
            }

            final pokemon = state.pokemon!;

            return Column(
              children: [
                AppBar(
                  iconTheme: const IconThemeData(color: Colors.white),
                  backgroundColor: ColorUtil.getTypeColor(
                    pokemon.types!.first.type.name,
                  ),
                  elevation: 0,
                ),
                Expanded(
                  child: Stack(
                    children: [
                      PokemonDetailBackground(pokemon: pokemon),
                      Stack(
                        children: [
                          const PokemonDetailTabBackground(),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                PokemonDetailHeader(pokemon: pokemon),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.width * 0.4,
                                  child: CachedImage(
                                    imageUrl:
                                        StringUtil().getImageUrl(pokemon.id!),
                                  ),
                                ),
                                PokemonDetailTab(pokemon: pokemon),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
