import 'package:dio/dio.dart';
import 'package:fluro/fluro.dart';
import 'package:pokedex/config/dio_config.dart';
import 'package:pokedex/config/routes/route_config.dart';

class App {
  static App? _instance;

  late FluroRouter router;
  late Dio dio;

  App._internal();

  factory App() {
    _instance ??= App._internal();

    return _instance!;
  }

  Future<void> init() async {
    router = FluroRouter();
    RouteConfig.configureRoutes(router);

    dio = await DioConfig().init();
  }
}
