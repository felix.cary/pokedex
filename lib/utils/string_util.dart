class StringUtil {
  static const baseUrl = "https://pokeapi.co/api/v2";

  static List<String> getParsedUrl(String url) {
    final urlWithoutBase = url.replaceFirst(baseUrl, '');
    final parts = urlWithoutBase.split('/');
    return [parts[1], parts[2]];
  }

  String capitalizeWords(String string) {
    List<String> words = string.split('-');
    words = words.map((word) => word.capitalize()).toList();
    return words.join(' ');
  }

  String getImageUrl(int pokemonId) {
    return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/$pokemonId.png";
  }

  String getPokemonNumber(int pokemonId) {
    return '#${pokemonId.toString().padLeft(3, '0')}';
  }
}

extension StringExtension on String {
  String capitalize() {
    if (isEmpty) {
      return this;
    }
    return this[0].toUpperCase() + substring(1);
  }
}
