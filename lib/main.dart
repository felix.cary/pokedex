import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:pokedex/app.dart';
import 'package:pokedex/bloc/pokemon_detail/pokemon_detail_cubit.dart';
import 'package:pokedex/services/api_services.dart';
import 'bloc/pokemon_list/pokemon_list_cubit.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await App().init();
  setupDependencyInjection();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then(
    (value) => runApp(
      const PokedexApp(),
    ),
  );
}

void setupDependencyInjection() {
  final getIt = GetIt.instance;

  getIt.registerLazySingleton(() => PokemonApiService(dioHttp: App().dio));
  getIt.registerFactory(
    () => PokemonListCubit(
      apiService: getIt.get<PokemonApiService>(),
    ),
  );
  getIt.registerFactory(
    () => PokemonDetailCubit(
      apiService: getIt.get<PokemonApiService>(),
    ),
  );
}

class PokedexApp extends StatelessWidget {
  const PokedexApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pokemon App',
      onGenerateRoute: App().router.generator,
    );
  }
}
