import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:pokedex/model/pokemon_list_model.dart';
import 'package:pokedex/services/api_services.dart';

part 'pokemon_list_state.dart';

class PokemonListCubit extends Cubit<PokemonListState> {
  final PokemonApiService apiService;
  final int pageSize = 25;
  int currentPage = 1;

  PokemonListCubit({
    required this.apiService,
  }) : super(const PokemonListState(pokemons: [], isLoading: false));

  void loadPokemons() async {
    if (state.isLoading) return;

    emit(state.copyWith(isLoading: true));

    try {
      final List<Pokemon> pokemons = await apiService.getPokemonList(
          currentPage: currentPage, pageSize: pageSize);

      emit(state.copyWith(
        pokemons: [...state.pokemons, ...pokemons],
        isLoading: false,
      ));

      currentPage++;
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }
}
