part of 'pokemon_list_cubit.dart';

class PokemonListState extends Equatable {
  final List<Pokemon> pokemons;
  final bool isLoading;

  const PokemonListState({
    required this.pokemons,
    required this.isLoading,
  });

  PokemonListState copyWith({
    List<Pokemon>? pokemons,
    bool? isLoading,
  }) {
    return PokemonListState(
      pokemons: pokemons ?? this.pokemons,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  List<Object?> get props => [pokemons, isLoading];
}
