part of 'pokemon_detail_cubit.dart';

class PokemonDetailState extends Equatable {
  final PokemonDetail? pokemon;
  final bool isLoading;

  const PokemonDetailState({
    this.pokemon,
    this.isLoading = false,
  });

  PokemonDetailState copyWith({
    PokemonDetail? pokemon,
    bool? isLoading,
  }) {
    return PokemonDetailState(
      pokemon: pokemon ?? this.pokemon,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  List<Object?> get props => [pokemon, isLoading];
}
