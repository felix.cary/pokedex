import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokedex/model/pokemon_detail_model.dart';
import 'package:pokedex/services/api_services.dart';

part 'pokemon_detail_state.dart';

class PokemonDetailCubit extends Cubit<PokemonDetailState> {
  final PokemonApiService apiService;

  PokemonDetailCubit({required this.apiService})
      : super(const PokemonDetailState());

  Future<void> loadPokemonDetail(String name) async {
    emit(state.copyWith(isLoading: true));

    try {
      final PokemonDetail pokemon =
          await apiService.getPokemonDetail(name: name);

      emit(state.copyWith(pokemon: pokemon, isLoading: false));
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }
}
