import 'package:dio/dio.dart';
import 'package:pokedex/utils/string_util.dart';

class DioConfig {
  Future<Dio> init() async {
    final BaseOptions options = BaseOptions(
      baseUrl: StringUtil.baseUrl,
      connectTimeout: const Duration(seconds: 30),
      receiveTimeout: const Duration(seconds: 30),
    );

    final Dio httpClient = Dio(options);

    return httpClient;
  }
}
