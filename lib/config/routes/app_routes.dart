import 'package:fluro/fluro.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:pokedex/bloc/pokemon_detail/pokemon_detail_cubit.dart';
import 'package:pokedex/bloc/pokemon_list/pokemon_list_cubit.dart';
import 'package:pokedex/config/routes/route_map.dart';
import 'package:pokedex/views/pokemon_detail/pokemon_detail_screen.dart';
import 'package:pokedex/views/pokemon_list/pokemon_list_screen.dart';

final RouteMap pokemonListRoute = RouteMap(
  '/',
  Handler(
    handlerFunc: (context, Map<String, List<String>> params) {
      return BlocProvider(
        create: (context) => GetIt.I.get<PokemonListCubit>(),
        child: const PokemonListScreen(),
      );
    },
  ),
);

final RouteMap pokemonDetailRoute = RouteMap(
  '/detail',
  Handler(
    handlerFunc: (context, Map<String, List<String>> params) {
      Map<String, dynamic> data =
          context!.settings!.arguments as Map<String, dynamic>? ?? {};
      String pokemonName = data['pokemonName'] ?? '';
      return BlocProvider(
        create: (context) =>
            GetIt.I.get<PokemonDetailCubit>()..loadPokemonDetail(pokemonName),
        child: const PokemonDetailScreen(),
      );
    },
  ),
);
