import 'package:fluro/fluro.dart';
import 'package:pokedex/config/routes/app_routes.dart';

class RouteConfig {
  static void configureRoutes(FluroRouter router) {
    pokemonListRoute.define(router);
    pokemonDetailRoute.define(router);
  }
}
